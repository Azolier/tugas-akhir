## Final Project
## Kelompok 4
## Anggota Kelompok

- Willson Tio
- Joan Antonio
- Arya Reza Inshafa

## Tema Project

E-Commerce

## ERD

<p align="center"><a href="#" target="_blank"><img src="https://i.ibb.co/R2pV2c3/ERD.png"></a></p>

## Link Demo Video

https://youtu.be/rkt_vxDYwPk