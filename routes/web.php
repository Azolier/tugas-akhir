<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/produk/create', 'ProdukController@create');
Route::post('/produk','ProdukController@store');
Route::get('/produk', 'ProdukController@index');
Route::get('/produk/{id}/edit','ProdukController@edit');
Route::put('/produk/{id}','ProdukController@update');
Route::delete('/produk/{id}','ProdukController@destroy');

Route::get('/profile/create', 'ProfileController@create');
Route::get('/profile/{id}', 'ProfileController@index');
Route::post('/profile/{id}','ProfileController@store');
Route::get('/profile/{id}/edit','ProfileController@edit');
Route::put('/profile/{id}','ProfileController@update');