<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProdukController extends Controller
{
    public function create() {
        return view('produk.create');
    }

    public function store(Request $request) {

        $request->validate([
            'namaproduk' => 'required',
            'harga' => 'required',      
        ]);
        $query = DB::table('produk')->insert(
            ["namaproduk" => $request["namaproduk"],
            "harga" => $request["harga"],
            ]
        );
        return redirect ('/produk')->with('success', 'Berhasil Menambah Produk!');
         
    }
    public function index() {
        $produk = DB::table('produk')->get(); //select * from
        // dd($produk);
        return view('produk.index', compact('produk'));
    }

    public function edit($id) {
        $produk = DB::table('produk')->where('id', $id)->first();
        return view('produk.edit', compact('produk'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'namaproduk' => 'required',
            'harga' => 'required', 
        ]);
        $query = DB::table('produk')
        ->where('id', $id)
        ->update([
            "namaproduk" => $request["namaproduk"],
            "harga" => $request["harga"]
        ]);
        return redirect ('/produk')->with('success', 'Berhasil Mengupdate Produk No '. $id);
    }

    public function destroy($id) {
        $produk = DB::table('produk')->where('id', $id)->delete();
        return redirect ('/produk')->with('success', 'Berhasil Menghapus Produk No '. $id);
    }

    public function test() {
        $produk = DB::table('produk')->get(); //select * from
        // dd($produk);
        return view('ecomm', compact('produk'));
    }
}
