@extends('adminlte.master')

@section('content')
        <div class ="ml-3 mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Produk {{$produk->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role ="form" action="/produk/{{$produk->id}}" method ="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="namaproduk">Nama Produk</label>
                    <input type="text" class="form-control" id="namaproduk" name="namaproduk" value="{{old('namaproduk', $produk->namaproduk)}}" placeholder="Masukkan Nama Produk">
                    @error('namaproduk')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga" value="{{old('harga', $produk->harga)}}"placeholder="Masukkan Harga">
                    @error('harga')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>                           
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
        </div>
@endsection