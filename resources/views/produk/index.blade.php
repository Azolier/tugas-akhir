@extends('adminlte.master')

@section('content')
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Produk</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                 <div class ="alert alert-success">
                    {{session('success')}}
                 </div>
              @endif
                <a href="/produk/create" class = "btn btn-primary mb-2">Tambah Produk Baru</a>
                @forelse($produk as $key => $keyproduk)
                    <div class="col-4 mt-2">
                    <div class="card" style="width: 18rem;">
                        <!-- <img src="${currentItem[4]}" class="card-img-top" height="200px" width="200px" alt="..."> -->
                        <div class="card-body">
                            <h5 class="card-title" id="itemName">{{$keyproduk->namaproduk}}</h5>
                            <!-- <p class="card-text" id="itemDesc">{{$keyproduk->namaproduk}}</p> -->
                            <p class="card-text">Rp. {{$keyproduk->harga}}</p>
                            <a href="#" class="btn btn-primary" id="addCart" onclick ="addCart()">Tambahkan ke keranjang</a>
                            <a href="/produk/{{$keyproduk->id}}/edit" class="btn btn-default mr-2">Edit</a>
                            <form action="/produk/{{$keyproduk->id}}" method = "POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class ="btn btn-danger">
                            </form>
                        </div>
                    </div>
                    </div>                  
                    @empty
                    <tr>
                    <td colspan="4" align="center">Tidak Ada produk</td>
                    </tr>
                    @endforelse
                <table class="table table-bordered">
                  <!-- <thead>
                    <tr>
                      <th style="width: 10px">No.</th>
                      <th>Nama Produk</th>
                      <th>Harga</th>
                      <th>Action</th>
                    </tr>
                  </thead> -->
                  <tbody>
                   
                  </tbody>
                </table>

                
                
                

                
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>
@endsection